<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmpresaRepository")
 */
class Empresa
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(name="telefone", type="string", length=255)
     */
    private $telefone;

    /**
     * @var string
     * @ORM\Column(name="cnpj", type="string", length=255)
     */
    private $cnpj;

    /**
     * @var Socio
     *
     * @ORM\ManyToOne(targetEntity="Socios")
     * @ORM\JoinColumn(name="Socio_id", referencedColumnName="id")
     */
    private $Socio;

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setCNPJ($cnpj)
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    public function getCNPJ()
    {
        return $this->cnpj;
    }


    public function getSocio()
    {
        return $this->Socio;
    }
}
